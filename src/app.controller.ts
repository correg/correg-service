import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { JWTPayload } from './libs/jwt-decoder/jwt-decoder.decorator'

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(@JWTPayload() payload: string): any {
    return {
      message: this.appService.getHello(),
      payload,
    }
  }
}
