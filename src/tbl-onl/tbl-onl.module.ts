import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TblOnlService } from './tbl-onl.service';
import { TblOnlController } from './tbl-onl.controller';
import { TblOnl } from './tbl-onl.entity';
import { TblCmr30 } from '../courses/entitiy/cmr30.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TblOnl, TblCmr30])],
  providers: [TblOnlService],
  controllers: [TblOnlController],
})
export class TblOnlModule {}
