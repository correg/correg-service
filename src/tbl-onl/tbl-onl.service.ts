import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { TblOnl } from './tbl-onl.entity';
import { AddCourseArgs } from './tbl-onl.dto';
import { TblCmr30 } from '../courses/entitiy/cmr30.entity';

@Injectable()
export class TblOnlService {
  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    @InjectRepository(TblCmr30)
    private readonly tblCmr30Entity: Repository<TblCmr30>,
    @InjectRepository(TblOnl)
    private readonly tblOnlEnitiy: Repository<TblOnl>,
  ) {}

  async addCourse(courseArgs: AddCourseArgs) {
    const queryRunner = this.connection.createQueryRunner();
    const { courseno, seclec, seclab } = courseArgs;

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const { affected } = await this.tblCmr30Entity.increment(
        {
          courseno,
          seclec,
          seclab,
        },
        'add_onl',
        1,
      );
      if (affected === 0) {
        throw new NotFoundException('course not found');
      }
      const result = await this.tblOnlEnitiy.insert(courseArgs);
      await queryRunner.commitTransaction();
      return result.identifiers[0];
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }
  }
}
