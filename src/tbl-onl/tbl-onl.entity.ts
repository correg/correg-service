import { Entity, Column, PrimaryColumn } from 'typeorm';
import { CourseBase } from '../libs/base.entity';

@Entity()
export class TblOnl extends CourseBase {
  @PrimaryColumn({ type: 'varchar', length: 1, name: 'regist_type' })
  registType: string;

  @PrimaryColumn({ type: 'varchar', length: 9, name: 'student_id' })
  studentId: string;

  @Column({ type: 'decimal', precision: 5, scale: 2 })
  crelec: number;

  @Column({ type: 'decimal', precision: 5, scale: 2 })
  crelab: number;

  @Column({ type: 'char', length: 3, name: 'seclec_move' })
  seclecMove: string;

  @Column({ type: 'char', length: 3, name: 'seclab_move' })
  seclabMove: string;

  @Column({ type: 'varchar', length: 20 })
  ip: string;

  @Column({ type: 'int', width: 11, name: 'random_no' })
  randomNo: number;

  @Column({ type: 'char', length: 2, name: 'reason_id' })
  reasonId: string;

  @Column({ type: 'int', width: 2, name: 'rank_no' })
  rankNo: number;
}
