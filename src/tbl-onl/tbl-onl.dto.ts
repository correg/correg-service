import { TblOnl } from './tbl-onl.entity';

export type AddCourseArgs = Omit<
  TblOnl,
  'inputDate' | 'seclecMove' | 'seclabMove' | 'reasonId' | 'randomNo'
>;

export type AddCourseBody = Pick<
  AddCourseArgs,
  'courseno' | 'seclec' | 'seclab' | 'crelec' | 'crelab'
>;
