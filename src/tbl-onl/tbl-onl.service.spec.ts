import { Test, TestingModule } from '@nestjs/testing';
import { TblOnlService } from './tbl-onl.service';

describe('TblOnlService', () => {
  let service: TblOnlService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TblOnlService],
    }).compile();

    service = module.get<TblOnlService>(TblOnlService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
