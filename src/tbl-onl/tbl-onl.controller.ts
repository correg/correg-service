import { Controller, Post, Get, Body } from '@nestjs/common';
import { TblOnlService } from './tbl-onl.service';
import { AddCourseBody } from './tbl-onl.dto';

@Controller('tbl-onl')
export class TblOnlController {
  constructor(private readonly tblOnlService: TblOnlService) {}

  @Post()
  addCourse(@Body() body: AddCourseBody) {
    /**
     * TODO: Make it complete
     *  1. sync this to cmu token
     *  2. count up add_onl in tbl_cmr30
     */
    return this.tblOnlService.addCourse({
      year: '2020',
      semester: '1',
      registType: 'A',
      inputUser: 'markmark',
      studentId: '600610773',
      ip: '0.0.0.0',
      rankNo: 1,
      ...body,
    });
  }
}
