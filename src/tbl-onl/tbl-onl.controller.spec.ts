import { Test, TestingModule } from '@nestjs/testing';
import { TblOnlController } from './tbl-onl.controller';

describe('TblOnlController', () => {
  let controller: TblOnlController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TblOnlController],
    }).compile();

    controller = module.get<TblOnlController>(TblOnlController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
