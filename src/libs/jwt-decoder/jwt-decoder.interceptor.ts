import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { Observable } from "rxjs";
import { JwtService } from '@nestjs/jwt'

@Injectable()
export class JWTDecodeInterceptor implements NestInterceptor {
  constructor(private jwtService: JwtService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest()

    const authHeader: string = request.headers['authorization']
    if (authHeader) {
      const token = authHeader.split(/[B|b]earer/)[1].trim()
      const payload = this.jwtService.decode(token)
      request.jwtPayload = payload
    }

    return next.handle()
  }
}