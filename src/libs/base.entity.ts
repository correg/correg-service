import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity()
export class ActionBase {
  @Column({ type: 'varchar', length: 50, name: 'input_user' })
  inputUser: string;

  @Column({ type: 'datetime', name: 'input_date' })
  inputDate: string;
}

@Entity()
export class CourseBase extends ActionBase {
  @PrimaryColumn({ type: 'varchar', length: 1 })
  semester: string;

  @PrimaryColumn({ type: 'varchar', length: 4 })
  year: string;

  @PrimaryColumn({ type: 'varchar', length: 6, name: 'courseno' })
  courseno: string;

  @PrimaryColumn({ type: 'varchar', length: 3 })
  seclec: string;

  @PrimaryColumn({ type: 'varchar', length: 3 })
  seclab: string;
}
