import { Injectable, OnModuleInit } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { Redis } from 'ioredis';
import { RandomStringService } from '@akanass/nestjsx-crypto';

@Injectable()
export class LockRedisService implements OnModuleInit {
  private client: Redis;

  constructor(
    private readonly redisService: RedisService,
    private readonly randomStringService: RandomStringService,
  ) {}

  private async random() {
    return this.randomStringService.generate().toPromise();
  }

  private getKey(resource: string) {
    return `lock:${resource}`;
  }

  onModuleInit() {
    this.client = this.redisService.getClient('main');
  }

  /**
   * TODO: Fix consistency of lock resource
   */
  async lock(resource: string, ms: number) {
    const lock = await this.random();
    const key = this.getKey(resource);
    const response = await this.client.setnx(key, lock);
    if (!response) throw Error(`${resource} resource is locked`);
    // await this.client.expire(resource, ms)
    return lock;
  }

  async release(resource: string, lock: string) {
    const key = this.getKey(resource);
    const value = await this.client.get(key);
    if (value !== lock) throw Error('wrong lock key');
    await this.client.del(key);
  }
}
