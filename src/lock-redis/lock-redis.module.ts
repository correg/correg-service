import { Module, Global } from '@nestjs/common';
import { CryptoModule } from '@akanass/nestjsx-crypto';
import { LockRedisService } from './lock-redis.service';

@Global()
@Module({
  imports: [CryptoModule],
  providers: [LockRedisService],
  exports: [LockRedisService],
})
export class LockRedisModule {}
