import { Test, TestingModule } from '@nestjs/testing';
import { LockRedisService } from './lock-redis.service';

describe('LockRedisService', () => {
  let service: LockRedisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LockRedisService],
    }).compile();

    service = module.get<LockRedisService>(LockRedisService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
