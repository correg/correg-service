import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisModule } from 'nestjs-redis';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TblOnlModule } from './tbl-onl/tbl-onl.module';
import { TblOnl } from './tbl-onl/tbl-onl.entity';
import { CoursesModule } from './courses/courses.module';
import { TblCmr30 } from './courses/entitiy/cmr30.entity';
import { TblBulletin } from './courses/entitiy/bulletin.entity';
import { LockRedisModule } from './lock-redis/lock-redis.module';
import { JWTDecodeInterceptor } from './libs/jwt-decoder/jwt-decoder.interceptor'
import { APP_INTERCEPTOR } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { TblInstructor } from './courses/entitiy/instructor.entity'
import { TblDay } from './courses/entitiy/day.entity';

@Module({
  imports: [
    RedisModule.register({
      name: 'main',
      url: 'redis://:asdf@redis-master:6379/',
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      url: 'mysql://root:asdf@mysql:3306',
      synchronize: false,
      entities: [TblOnl, TblCmr30, TblBulletin, TblInstructor, TblDay],
    }),
    TblOnlModule,
    CoursesModule,
    LockRedisModule,
    JwtModule.register({}),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: JWTDecodeInterceptor,
    }
  ],
})
export class AppModule {}
