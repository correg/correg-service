import { Controller, Get } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { CoursesService } from './courses.service';

@Controller('courses')
export class CoursesController {
  constructor(
    private readonly coursesService: CoursesService,
    private readonly redisService: RedisService,
  ) {}

  @Get('cache')
  async courses() {
    const redis = this.redisService.getClient('main');
    const cache = await redis.exists('courses');
    let courses;

    if (!cache) {
      console.log('try to lock');
      const lock = await redis.setnx('lock:courses', 'locking');

      if (!!lock) {
        console.log('query hits');
        courses = await this.coursesService.getCourses();
        await redis.set('courses', JSON.stringify(courses), 'ex', 1);

        await redis.del('lock:courses');
        return courses;
      }

      console.log('lock failed');
    }

    console.log('cache hits');
    // const loop = async () => {
    //   let courses
    while (true) {
      courses = JSON.parse(await redis.get('courses'));
      if (courses) return courses;
    }
    // }
    // courses = JSON.parse(await redis.get('courses'))
    // if (!courses) throw Error('Courses is null')
    // return loop()
  }

  @Get()
  async testCourses() {
    console.log('query hits');
    return this.coursesService.getCourses();
  }
}
