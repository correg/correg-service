import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ElasticsearchModule } from '@nestjs/elasticsearch';
import { CoursesService } from './courses.service';
import { CoursesController } from './courses.controller';
import { CoursesCacheService } from './courses.cache.service'
import { TblCmr30 } from './entitiy/cmr30.entity';
import { TblBulletin } from './entitiy/bulletin.entity';
import { TblInstructor } from './entitiy/instructor.entity';
import { TblDay } from './entitiy/day.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([TblCmr30, TblBulletin, TblInstructor, TblDay]),
    // TypeOrmModule.forFeature([TblBulletin, TblInstructor, TblDay], 'db_center_connection'),
    ElasticsearchModule.register({
      node: 'http://elasticsearch-master:9200',
    }),
  ],
  providers: [CoursesService, CoursesCacheService],
  controllers: [CoursesController],
})
export class CoursesModule {}
