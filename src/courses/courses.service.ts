import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository, InjectConnection } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { RedisService } from 'nestjs-redis';
import { Redis } from 'ioredis';
import { TblCmr30 } from './entitiy/cmr30.entity';
import { TblBulletin } from './entitiy/bulletin.entity'
import { TblInstructor } from './entitiy/instructor.entity'
import { TblDay } from './entitiy/day.entity'
import { CoursesStatDict } from './courses-stats-dict.object';
import { CoursesCacheService } from './courses.cache.service';

export type CourseStat = Pick<
  TblCmr30,
  'courseno' | 'seclec' | 'seclab' | 'add_onl' | 'drop_onl'
>;

export interface CourseStatKey {
  courseno: string;
  seclec: string;
  seclab: string;
}

export type CourseStatValue = Pick<TblCmr30, 'add_onl' | 'drop_onl'>;

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(TblCmr30)
    private readonly tblCmr30Entity: Repository<TblCmr30>,
    @InjectConnection()
    private readonly connection: Connection,
    private readonly coursesCacheService: CoursesCacheService,
  ) {}

  async getCourses(courseKeys?: CourseStatKey[]) {
    /**
     * TODO: get condition from tbl_condition
     *       and get time2 from tbl_cmr30_time 
     */

    if (courseKeys.length === 0) return []
    return this.connection
      .getRepository(TblCmr30)
      .createQueryBuilder('cmr30')
      .select([
        'cmr30.courseno',
        'bulletin.title_short_en as title',
        'cmr30.seclec',
        'cmr30.seclab',
        'bulletin.crelec',
        'bulletin.crelab',
        'day.day_name',
        'cmr30.study_style',
        'cmr30.btime',
        'cmr30.ftime',
        'cmr30.room',
        'instructor.name_en',
        'instructor.surname_en',
        'cmr30.max',
        'cmr30.regist_all',
        'cmr30.add_onl',
        'cmr30.drop_onl',
        'cmr30.move_in_onl',
        'cmr30.move_out_onl'
      ])
      .leftJoin('tbl_bulletin', 'bulletin', 'cmr30.bulletin_id = bulletin.bulletin_id')
      .leftJoin('tbl_instructor', 'instructor', 'cmr30.instructor_id = instructor.instructor_id')
      .leftJoin('tbl_day', 'day', 'cmr30.day_id = day.day_id')
      // .orderBy('courseno', 'ASC')
      // .orderBy('seclec', 'ASC')
      // .orderBy('seclab', 'ASC')
      .where(courseKeys)
      .getMany()
  }

  /**
   * Retrive course stats from mysql and cache in redis
   */
  async retriveCourseStatsAndCache(
    courseKeys: CourseStatKey[]
  ): Promise<CoursesStatDict> {
    /**
     * Base case
     */
    if (courseKeys.length === 0) 
      return new CoursesStatDict()
    
    const stats: CourseStat[] = await this.tblCmr30Entity.find({
      select: ['courseno', 'seclec', 'seclab', 'add_onl', 'drop_onl'],
      where: courseKeys,
    })

    this.coursesCacheService.cacheCourseStats(stats);

    return stats.reduce(
      (prev, { add_onl, drop_onl, ...args }) =>
        prev.set(args, { add_onl, drop_onl }),
      new CoursesStatDict(),
    );
  }

  /**
   * This method get stats such as add_onl, drop_onl etc
   * @param courseKeys
   */
  async getCourseStats(
    courseKeys: CourseStatKey[],
  ): Promise<CoursesStatDict> {
    const [statCaches, notBulletinCacheIds] = await this.coursesCacheService.getCourseStatCaches(
      courseKeys,
    );

    console.log('not cached', notBulletinCacheIds);
    const statsDict = await this.retriveCourseStatsAndCache(notBulletinCacheIds)

    const mergedStatsDict = new CoursesStatDict(
      statCaches,
      statsDict,
    );

    return mergedStatsDict;
  }
}
