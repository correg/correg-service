import { DefaultValuePipe, OnModuleInit, ParseIntPipe } from '@nestjs/common';
import { Controller, Get, Query } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { RedisService } from 'nestjs-redis';
import { LockRedisService } from '../lock-redis/lock-redis.service';
import { CoursesService, CourseStatKey } from './courses.service';
import { Redis } from 'ioredis';
import { TblCmr30 } from './entitiy/cmr30.entity';

@Controller('courses')
export class CoursesController implements OnModuleInit {
  private redis: Redis;
  constructor(
    private readonly coursesService: CoursesService,
    private readonly redisService: RedisService,
    private readonly lockRedisService: LockRedisService,
    private readonly elasticsearchService: ElasticsearchService,
  ) {}

  onModuleInit() {
    this.redis = this.redisService.getClient('main');
  }

  /**
   * Experimental of get courses cache with lock resource redis
   */
  @Get('cache')
  async getCouresAndCache() {
    const cache = await this.redis.exists('courses');

    if (cache) return JSON.parse(await this.redis.get('courses'));

    try {
      console.log('try to lock');
      const lock = await this.lockRedisService.lock('courses', 1000);

      console.log('query hits');
      const courses = await this.coursesService.getCourses();
      await this.redis.set('courses', JSON.stringify(courses), 'px', 1000);

      await this.lockRedisService.release('courses', lock);
      return courses;
    } catch (e) {
      if (!e.message.match(/locked/)) throw e;

      /**
       * Retry to get cache
       */
      while (true) {
        const courses = JSON.parse(await this.redis.get('courses'));
        if (courses) return courses;
      }
    }
  }

  @Get('withoutCache')
  async getCourses() {
    console.log('query hits');
    return this.coursesService.getCourses();
  }

  // @Get('search')
  async searchCourses(@Query('q') q: string) {
    /**
     * Search courses
     */
    const response = await this.elasticsearchService.search({
      index: 'cmr30',
      q,
      size: 20,
    });
    const rawCourses: any[] = response.body.hits.hits;
    const courses: TblCmr30[] = rawCourses.map(raw => raw._source);

    const courseKeys: CourseStatKey[] = courses.map(
      ({ courseno, seclec, seclab }) => ({
        courseno,
        seclec,
        seclab,
      }),
    );

    const stats = await this.coursesService.getCourseStats(courseKeys);
    console.log(stats);

    const mergedCourses = courses.map(course => {
      const { courseno, seclec, seclab } = course;
      return {
        ...course,
        ...stats.get({ courseno, seclec, seclab }),
      };
    });

    return mergedCourses;
  }

  @Get('search')
  async searchCoursesWithoutCache(
    @Query('q') q: string,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
  ) {
    const per_page = 12
    /**
     * counting start at 0
     */
    const from = per_page * (page - 1)

    const { body } = await this.elasticsearchService.search({
      index: 'cmr30',
      q,
      size: per_page,
      from,
    });

    const rawCourses: any[] = body.hits.hits;
    let courses: TblCmr30[] = rawCourses.map(raw => raw._source);

    const courseKeys: CourseStatKey[] = courses.map(
      ({ courseno, seclec, seclab }) => ({
        courseno,
        seclec,
        seclab,
      }),
    );

    const fullCourses = await this.coursesService.getCourses(courseKeys)

    courses = courses.map(course => {
      const findedCourse = fullCourses.find(item => item.courseno === course.courseno 
        && item.seclec === course.seclec 
        && item.seclab === course.seclab)
      return {
        ...course,
        ...findedCourse,
      }
    })
    const total = body.hits.total.value
    const to = per_page * page < total ? per_page * page : total


    return {
      total,
      data: courses,
      per_page,
      /**
       * counting start at 1
       */
      from: from + 1,
      to,
      last_page: Math.ceil(body.hits.total.value / per_page)
    }
  }
}
