import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class TblExam {
  @PrimaryColumn()
  courseno: string;

  @PrimaryColumn()
  examcode: string;

  @Column()
  type: string;
}

@Entity()
export class TblExamDate {
  @PrimaryColumn()
  examcode: string;

  @Column()
  date: string;

  @Column()
  month: string;

  @Column()
  year: string;

  @Column()
  day: string;

  @Column()
  time: string;

  @Column()
  note: string;

  @Column()
  term: string;

  @Column()
  type: string;
}
