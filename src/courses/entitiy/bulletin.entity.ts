import { Entity, Column, PrimaryColumn } from 'typeorm';

/**
 * TODO: This entity use to retrive data for regist service
 *       Please see full mysql schema in database
 */
@Entity({ name: 'tbl_bulletin', database: 'db_regist' })
export class TblBulletin {
  @PrimaryColumn({ type: 'varchar', name: 'bulletin_id', unique: true })
  bulletinId: string;

  @Column({ type: 'varchar', name: 'courseno' })
  courseNo: string;

  @Column({ type: 'varchar', name: 'courseno_en' })
  courseNoEn: string;

  @Column({ type: 'varchar', name: 'courseno_th' })
  courseNoTh: string;

  @Column({ type: 'varchar', name: 'title_short_en' })
  titleShortEn: string;

  @Column({ name: 'title_short_th' })
  titleShortTh: string;

  @Column({ name: 'title_long_th' })
  titleLongTh: string;

  @Column({ name: 'degree_level' })
  degree_level: string;

  @Column({ name: 'course_type' })
  courseType: string;

  @Column({ name: 'crelec' })
  crelec: number;

  @Column({ name: 'crelab' })
  crelab: number;

  @Column({ name: 'thesis_is' })
  thesisIs: string;
}
