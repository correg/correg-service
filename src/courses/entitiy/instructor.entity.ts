import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'tbl_instructor', database: 'db_center' })
export class TblInstructor {
  @PrimaryColumn()
  instructor_id: string;

  @Column()
  name_en: string;

  @Column()
  surname_en: string;

  @Column()
  name_th: string;

  @Column()
  surname_th: string;
}
