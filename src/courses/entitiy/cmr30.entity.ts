import { Entity, Column } from 'typeorm';
import { CourseBase } from '../../libs/base.entity';

@Entity({ name: 'tbl_cmr30', database: 'db_regist' })
export class TblCmr30 extends CourseBase {
  @Column({ type: 'smallint', default: 0 })
  max: number;

  @Column({ type: 'smallint', default: 0, name: 'regist_all' })
  registAll: number;

  @Column({ type: 'char', length: 1, name: 'status' })
  status: string;

  @Column({ type: 'smallint', width: 6, name: 'add_onl' })
  add_onl: number;

  @Column({ type: 'smallint', width: 6, name: 'drop_onl' })
  drop_onl: number;

  @Column({ type: 'smallint', width: 6, name: 'move_in_onl' })
  move_in_onl: number;

  @Column({ type: 'smallint', width: 6, name: 'move_out_onl' })
  move_out_onl: number;

  @Column({ type: 'varchar', length: 100 })
  room: string;

  @Column({ type: 'char', length: 2, name: 'day_id' })
  dayId: string;

  @Column({ type: 'char', length: 4, name: 'btime' })
  beginTime: string;

  @Column({ type: 'char', length: 4, name: 'ftime' })
  finishTime: string;

  @Column({ type: 'varchar', length: 15, name: 'instructor_id' })
  instructor_id: string;

  @Column({ type: 'varchar', length: 8, name: 'bulletin_id' })
  bulletin_id: string;

  @Column({
    type: 'varchar',
    length: 1,
    default: 'Y',
    name: 'canchangebulletin',
  })
  canChangeBulletin: string;

  @Column({ type: 'char', length: 1, name: 'study_type' })
  studyType: string;

  @Column({ type: 'char', length: 1, name: 'study_style' })
  studyStyle: string;

  @Column({ type: 'varchar', length: 2, name: 'faculty_id' })
  facultyId: string;

  @Column({ type: 'char', length: 1 })
  smile: string;

  @Column({ type: 'int', width: 11, name: 'smile_max' })
  smileMax: number;
}
