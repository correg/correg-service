import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'tbl_day', database: 'db_center' })
export class TblDay {
  @PrimaryColumn()
  day_id: string;

  @Column()
  day_name: string;

  @Column()
  day_detail: string;
}
