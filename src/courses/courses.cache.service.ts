import {
  Injectable,
  NotImplementedException,
  OnModuleInit,
} from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { Redis } from 'ioredis';
import { TblCmr30 } from './entitiy/cmr30.entity';
import { CoursesStatDict, CourseStatKey, CourseStatValue } from './courses-stats-dict.object';

export type CourseStat = Pick<
  TblCmr30,
  'courseno' | 'seclec' | 'seclab' | 'add_onl' | 'drop_onl'
>;


@Injectable()
export class CoursesCacheService implements OnModuleInit {
  private redis: Redis;

  constructor(private readonly redisService: RedisService) {}

  onModuleInit() {
    this.redis = this.redisService.getClient('main');
  }

  private getCourseStatKeyCache({
    courseno,
    seclec,
    seclab,
  }: Pick<CourseStat, 'courseno' | 'seclec' | 'seclab'>) {
    return `courses_stat:${courseno}:${seclec}:${seclab}`;
  }

  async getCourseStatCaches(
    courseKeys: CourseStatKey[],
  ): Promise<
    [CoursesStatDict, CourseStatKey[]]
  > {
    /**
     * Base case: empty list
     */
    if (courseKeys.length === 0) return [new CoursesStatDict(), []];

    /**
     * trim() Fix elasticsearch value
     */
    const bulletinKeyCaches = courseKeys.map(args => this.getCourseStatKeyCache(args));
    console.log('keys', bulletinKeyCaches);

    /**
     * TODO: locks redis for load data from mysql
     */
    const rawStatCaches = await this.redis.mget(bulletinKeyCaches);
    console.log('raw', rawStatCaches);

    const statCaches = rawStatCaches
      .filter(raw => !!raw)
      .reduce<CoursesStatDict>(
        (prev, raw, index) => prev.set(courseKeys[index], JSON.parse(raw)),
        new CoursesStatDict(),
      );
    console.log('cache', statCaches);

    const notCacheIds = rawStatCaches.reduce<CourseStatKey[]>(
      (prev, raw, index) => (raw ? prev : [...prev, courseKeys[index]]),
      [],
    );

    return [statCaches, notCacheIds];
  }

  async cacheCourseStats(courseStats: CourseStat[]) {
    const bulletinCaches = courseStats.map(
      ({ add_onl, drop_onl, ...args }) => ({
        key: this.getCourseStatKeyCache(args),
        add_onl,
        drop_onl,
      }),
    );
    console.log(bulletinCaches);

    /**
     * TODO: should set little ttl (near 500ms)
     */
    console.warn('Please set ttl in stats');

    const trans = this.redis.multi();
    bulletinCaches.forEach(async ({ key, ...args }) => {
      // console.log(key, args)
      trans.set(key, JSON.stringify(args), 'ex', 5);
    });
    const response = await trans.exec();
    console.log('res', response);
  }

  incrementAddOnlCourse(courseno: string): Promise<void> {
    throw new NotImplementedException();
  }

  incrementDropOnlCourse(courseno: string): Promise<void> {
    throw new NotImplementedException();
  }
}
