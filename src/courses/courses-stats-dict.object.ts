import { TblCmr30 } from './entitiy/cmr30.entity'

export interface CourseStatKey {
  courseno: string;
  seclec: string;
  seclab: string;
}

export type CourseStatValue = Pick<TblCmr30, 'add_onl' | 'drop_onl'>;

export class CoursesStatDict {
  private map: Map<string, CourseStatValue>;

  constructor(...args: CoursesStatDict[]) {
    this.map = args.reduce(
      (prev, item) => new Map<string, CourseStatValue>([...prev, ...item.toValue()]),
      new Map<string, CourseStatValue>(),
    );
  }

  getKey({ courseno, seclec, seclab }: CourseStatKey) {
    return `${courseno}:${seclec}:${seclab}`
  }

  set(key: CourseStatKey, value: CourseStatValue) {
    this.map.set(this.getKey(key), value);
    return this;
  }

  get(key: CourseStatKey) {
    return this.map.get(this.getKey(key));
  }

  toValue() {
    return this.map;
  }
}
